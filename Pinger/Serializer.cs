﻿using Newtonsoft.Json.Converters;
using System;
using System.Text;
namespace Pinger
{
    public static class Serializer
    {
        public static string Serialize(Object data)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(data);
        }
        public static T Deserialize<T>(string data, string dateFormat = null)
        {
            if (string.IsNullOrWhiteSpace(dateFormat))
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);
            else
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data, new IsoDateTimeConverter { DateTimeFormat = dateFormat });
        }

        public static string EncodeTo64(string data)
        {
            return Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(data));
        }
        public static string DecodeFrom64(string data)
        {
            return UTF8Encoding.UTF8.GetString(Convert.FromBase64String(data));
        }
    }
}