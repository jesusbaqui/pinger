﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.CursorVisible = false;

            var configFileName = "Config.json";
            var jsonPath = AppDomain.CurrentDomain.BaseDirectory + "\\" + configFileName;
            var config = new Config();

            if (File.Exists(jsonPath))
            {
                using (var urisFileStream = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "\\" + configFileName, true))
                {
                    var jsonString = urisFileStream.ReadToEnd();
                    config = Serializer.Deserialize<Config>(jsonString);
                    urisFileStream.Close();
                }

                if (config.Uris.Count == 0)
                {
                    Console.WriteLine("No uris specified in config file 'Config.json'");
                }
            }
            else
            {
                Console.WriteLine("No 'Config.json' file found, creating...");

                using (var urisFileStream = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\" + configFileName, true, Encoding.UTF8))
                {
                    var jsonString = Serializer.Serialize(config);
                    urisFileStream.Write(jsonString);
                    urisFileStream.Close();
                }
            }


            if (config.SecondsInterval == 0)
                config.SecondsInterval = 60;

            if (config.Uris.Count > 0)
                Pinger(config.Uris, config.SecondsInterval).Wait();

            Console.CursorVisible = true;
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        static async Task<bool> Pinger(List<string> uris, int secondsInternal, float goodCounter = 0, float badCounter = 0)
        {
            foreach (var uri in uris)
            {
                if (string.IsNullOrWhiteSpace(uri))
                    continue;

                try
                {
                    using (var webClient = new WebClientExtended())
                    {
                        var stringDownlaodReponse = await webClient.DownloadStringTaskAsync(new Uri(uri));

                        if (string.IsNullOrWhiteSpace(stringDownlaodReponse))
                        {
                            Log("Null response while pinging to " + uri);
                            badCounter++;
                        }

                        goodCounter++;
                    }
                }
                catch (Exception ex)
                {
                    badCounter++;
                    Log("Error pinging " + uri + ", message: " + ex.ToString());
                }

                Console.Clear();
                Console.WriteLine($"Pinging to uris: ");
                var urisCounter = 0;
                foreach (var url in uris)
                {
                    Console.WriteLine($"{++urisCounter} - {url}");
                }

                Console.WriteLine();
                Console.WriteLine($"Progress - OK: {goodCounter} | KO: {badCounter}");
            }

            await Task.Delay(TimeSpan.FromSeconds(secondsInternal));

            await Pinger(uris, secondsInternal, goodCounter, badCounter);

            return true;
        }

        public static async void Log(string texto)
        {
            System.Diagnostics.Debug.WriteLine(texto);

            var mensaje = string.Empty;

            var textoFecha = DateTime.Now.ToString("dd/MM/yyyy HH:mm.");

            mensaje = char.ConvertFromUtf32(13) + char.ConvertFromUtf32(10) + textoFecha + ". " + texto + char.ConvertFromUtf32(13) + char.ConvertFromUtf32(10);

            try
            {
                using (var archivoLogs = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Log.txt", true, Encoding.UTF8))
                {
                    await archivoLogs.WriteAsync(mensaje);
                    archivoLogs.Close();
                }
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("NO SE PUDO GUARDAR LOG EN ARCHIVO...");
            }
        }
    }

    public class Config
    {
        public int SecondsInterval { set; get; } = 30;
        public List<string> Uris { set; get; } = new List<string>();
    }
}
