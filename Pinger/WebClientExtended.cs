﻿using System;
using System.Net;
using System.Text;

namespace Pinger
{
    public class WebClientExtended : WebClient
    {
        public int Timeout { get; set; }

        public WebClientExtended() : this(TimeSpan.FromSeconds(4).Milliseconds) { }

        public WebClientExtended(int timeout)
        {
            Timeout = timeout;
            Encoding = Encoding.UTF8;
            Headers.Add(HttpRequestHeader.UserAgent, "Pinger");
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address);
            if (request != null)
            {
                request.Timeout = Timeout;
            }
            return request;
        }
    }
}